package co.id.medcare;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

public class ObatAdapter extends RecyclerView.Adapter<ObatAdapter.ViewHolder> {
    private ArrayList<HashMap<String, String>> postList;
    private ObatActivity activity;

    public ObatAdapter(ArrayList<HashMap<String, String>> postList, ObatActivity obatActivity) {
        this.postList = postList;
        this.activity = obatActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_obat_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final HashMap<String, String> post = postList.get(position);
        Glide.with(activity).load(post.get("logo")).into(viewHolder.imgLogo);
        viewHolder.textNamaUniv.setText(post.get("nama"));
        viewHolder.textNoTelp.setText(post.get("telepon"));
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgLogo;
        TextView textNamaUniv;
        TextView textNoTelp;

        public ViewHolder(View view) {
            super(view);
            imgLogo = (ImageView) view.findViewById(R.id.imgLogo);
            textNamaUniv = (TextView) view.findViewById(R.id.namaUnivText);
            textNoTelp = (TextView) view.findViewById(R.id.noTelpText);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final HashMap<String, String> post = postList.get(getAdapterPosition());

            Intent intent = new Intent(activity, DetailActivity.class);
            intent.putExtra("map", post);
            activity.startActivity(intent);
        }
    }
}
