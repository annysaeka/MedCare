package co.id.medcare;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Annysa Eka on 5/12/2017.
 */

public class TabFragmentAdapter extends FragmentPagerAdapter {
    //nama tab nya
    String[] title = new String[]{
            "PROFILE", "NEWS"
    };

    public TabFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment dilayar
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new Tab1Fragment();
                break;
            case 1:
                fragment = new Tab2Fragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    public int getCount() {
        return title.length;
    }

}
