package co.id.medcare;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import co.id.medcare.ObatActivity;
import co.id.medcare.R;

public class Tab2Fragment extends Fragment {

    ListView listView;
    String arrMenu[] = {"1. Obat", "2. Obat2", "3. Obat3", "4. Obat5", "5. Obat6"};

    public Tab2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tab2_fragment, container, false);
        listView = (ListView) view.findViewById(R.id.listView);

        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView.setAdapter(
                new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, arrMenu ));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    startActivity(new Intent(getActivity(), ObatActivity.class));
                }else if (position == 1){
                    Toast.makeText(getActivity(), "Masih Belum", Toast.LENGTH_LONG).show();
                }else if (position == 2){
                    Toast.makeText(getActivity(), "Masih Belum", Toast.LENGTH_LONG).show();
                }else if (position == 3){
                    Toast.makeText(getActivity(), "Masih Belum", Toast.LENGTH_LONG).show();
                }else if (position == 4){
                    Toast.makeText(getActivity(), "Masih Belum", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
