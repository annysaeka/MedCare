package co.id.medcare;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;

public class DetailActivity extends AppCompatActivity {

    ImageView imageLogo;
    TextView textNamaUniv, textNoTelp, textAlamat, textDeskripsi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setTitle("Detail Kampus");

        imageLogo = (ImageView) findViewById(R.id.imageLogoUniv);
        textNamaUniv = (TextView) findViewById(R.id.textNamaUniv);
        textNoTelp = (TextView) findViewById(R.id.textTelp);
        textAlamat = (TextView) findViewById(R.id.textAlamat);
        textDeskripsi = (TextView) findViewById(R.id.textDeskripsi);

        Intent intent = getIntent();
        HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("map");
        Glide.with(this).load(hashMap.get("logo")).into(imageLogo);

        textNamaUniv.setText(hashMap.get("nama"));
        textNoTelp.setText(hashMap.get("telepon"));
        SpannableString content = new SpannableString(hashMap.get("alamat"));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textAlamat.setText(content);
        textDeskripsi.setText(hashMap.get("deskripsi"));
    }

    public void openMap(View v) {
        String map = "http://maps.google.co.in/maps?q=" + textAlamat.getText().toString().trim();

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
        startActivity(i);
    }
}
